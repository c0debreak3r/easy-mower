package fr.upem


object CharUtils {
  import fr.upem.Classes._

  implicit class CharUtils(val char: Char) {
    def toOptionalAction: Option[Action] = char match {
      case 'A' => Some(Advance)
      case 'G' => Some(Left)
      case 'D' => Some(Right)
      case _ => None
    }
  }
}
