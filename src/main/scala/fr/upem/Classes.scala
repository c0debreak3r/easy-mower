package fr.upem

object Classes {

  sealed trait Direction

  case object East extends Direction

  case object West extends Direction

  case object North extends Direction

  case object South extends Direction


  sealed trait Action

  case object Advance extends Action

  case object Left extends Action

  case object Right extends Action


  case class Grassland(val limits: (Int, Int))

  case class Position(val coordinates: (Int, Int), val direction: Direction)

  case class Lawnmower(val position: Position, val actions: List[Action])

  case class Configuration(val grassland: Grassland, val lawnmowers: List[Lawnmower])
}
