package fr.upem


object StringUtils {
  import Classes._
  import CharUtils._

  implicit class StringUtils(val string: String) {
    def toOptionalInt: Option[Int] = {
      try {
        Some(string.toInt)
      } catch {
        case e: Exception => None
      }
    }

    def toOptionalDirection: Option[Direction] = string match {
      case "E" => Some(East)
      case "W" => Some(West)
      case "N" => Some(North)
      case "S" => Some(South)
      case _ => None
    }

    def toOptionalPosition: Option[Position] = string.split(' ') match {
      case Array(xStr, yStr, directionStr) => (xStr.toOptionalInt, yStr.toOptionalInt, directionStr.toOptionalDirection) match {
        case (Some(x), Some(y), Some(direction)) => Some(Position((x, y), direction))
        case _ => None
      }
      case _ => None
    }

    def toOptionalActions: Option[List[Action]] = string.toList match {
      case Nil => None
      case list => list.map(actionChar => actionChar.toOptionalAction) match {
        case list if !list.contains(None) => Option(list.flatten)
        case _ => None
      }
    }

    def toOptionalGrassland: Option[Grassland] = string.split(' ') match {
      case Array(xLimitStr : String, yLimitStr : String) => (xLimitStr.toOptionalInt, yLimitStr.toOptionalInt) match {
        case (Some(xLimit), Some(yLimit)) if xLimit > 0 && yLimit > 0 => Some(Grassland((xLimit, yLimit)))
        case _ => None
      }
      case _ => None
    }
  }
}
