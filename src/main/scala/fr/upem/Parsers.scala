package fr.upem


object Parsers {
  import Classes._
  import Utils._
  import StringUtils._
  import ListUtils._

  def parseLawnmower(positionLine: String, actionsLine: String, limits: (Int, Int)): Option[Lawnmower] = (positionLine.toOptionalPosition, actionsLine.toOptionalActions) match {
    case (Some(position), Some(actions)) if checkCoordinates(position.coordinates, limits) => Some(Lawnmower(position, actions))
    case _ => None
  }

  def parseLawnmowersUtil(lawnmowerStrList: List[String], limits: (Int, Int)): List[Option[Lawnmower]] = lawnmowerStrList match {
    case Nil => Nil
    case _ :: Nil => None :: Nil
    case positionLine :: actionsLine :: rest => parseLawnmower(positionLine, actionsLine, limits) :: parseLawnmowersUtil(rest, limits)
  }

  def parseLawnmowers(lawnmowerStrList: List[String], limits: (Int, Int)): Option[List[Lawnmower]] = flattenList(parseLawnmowersUtil(lawnmowerStrList, limits))

  def parseFile(filename: String): Option[Configuration] = getLines(filename) match {
    case Some(lines) => lines match {
      case grasslandLine :: lawnmowerStrList => grasslandLine.toOptionalGrassland match {
        case Some(grassland) => parseLawnmowers(lawnmowerStrList, grassland.limits) match {
          case Some(lawnmowers) => Some(Configuration(grassland, lawnmowers))
          case _ => None
        }
        case _ => None
      }
      case _ => None
    }
    case _ => None
  }
}
