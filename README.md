# Easy Mower project

## Introduction
The goal of this project is to correctly parse a text file containing a grassland and lawnmowers data and computing the final position of each lawnmower.

We considered in the implementation that a lawnmower isn't added to the grassland until the one ahead of it in the list has executed all its actions. Therefore, we don't need to check if lawnmower positions overlap when parsing the input file. We also need to ensure a lawnmower can't move to a square already occupied by another one.


##  Documentation
File | Informations
------|--------------
Main | The main class
Classes | Contains all the classes used in the project
Parsers | Methods used to parse lawnmowers
Utils | Other methods used in the project
CharUtils | Utility methods added to Char (parsing Action from a Char)
StringUtils | Utility methods added to String (parsing other classes)
ListUtils | Contains a method that converts a List[Option[A]] to Option[List[A]]

## Compiling and executing the project
### Compilation
```
#sbt compile
```   

### Execution
The main class needs a filename as an argument
```
#sbt "run filename"
```   
