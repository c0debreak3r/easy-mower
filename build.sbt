name := "EasyMow"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "3.0.1" % "test"

Compile/mainClass := Some("fr.upem.Main")